data "tfe_outputs" "pet" {
    organization = "ivan_test"
    workspace = "random_pet_source"
    }
  

resource "null_resource" "pet" {
 provisioner "local-exec" {
    command = "echo ${data.tfe_outputs.pet.values.pet_name}"
  }
}

